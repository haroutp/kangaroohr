﻿using System;

namespace Kangaroo
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(kangaroo(0, 3, 4, 2));
            System.Console.WriteLine(kangaroo(0, 2, 5, 3));

        }

        static string kangaroo(int x1, int v1, int x2, int v2) {

            if(v1 > v2){
                int remainder = (x2 - x1) % (v1 - v2);

                if(remainder == 0){
                    return "YES";
                }else{
                    return "NO";
                }
            }
            
            return "NO";
            // velocity = distance / time BUT HERE velocity = distance / jumps
            // distance = velocity * jumps
            // x1 + v1 * j = x2 + v2 * j
            // v1j - v2j = x2 - x1
            // j(v1 - v2) = x2 - x1
            // NumberOfJumps or j = (x2 - x1) / (v1 - v2)
            // for them to reach the same spot the remainder should be zero

            
        }
    }
}
